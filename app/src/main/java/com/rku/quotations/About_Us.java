package com.rku.quotations;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class About_Us extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about__us);
        setTitle("About Us");
        TextView textView;
        textView = findViewById(R.id.version);
        textView.setText("Version : "+String.valueOf(BuildConfig.VERSION_NAME));
    }
}